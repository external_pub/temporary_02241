import json
import types
from collections import deque
from itertools import chain


class Component:
    def __init__(self, *algorithm_list):
        self.algorithm_list = algorithm_list

    def __call__(self, source_object):
        result = []
        queue = deque([source_object])

        while queue:
            result.extend(queue)
            queue = list(chain.from_iterable(
                algorithm(item)
                for item in queue
                for algorithm in self.algorithm_list
            ))

        return result

    @staticmethod
    def func_name(algorithm):
        return algorithm.__name__ if isinstance(algorithm, types.FunctionType) else algorithm.__class__.__name__

    @staticmethod
    def target_classes(source_obj, algorithm):
        return algorithm.SPECIFICATION.get(source_obj, [])

    def search_algorithm(self, source_class):
        results = {
            'Potential': set(),
            'Algorithm': {algo: {} for algo in map(self.func_name, self.algorithm_list)}
        }
        queue = deque([(source_class.__name__, source_class)])

        while queue:
            path, source = queue.popleft()
            results['Potential'].add(path)

            for algorithm in self.algorithm_list:
                target_classes = self.target_classes(source, algorithm)
                algo_name = self.func_name(algorithm)
                for target_class in target_classes:
                    new_path = f"{path}/{target_class.__name__}"
                    if new_path not in results['Potential']:
                        queue.append((new_path, target_class))
                    results['Algorithm'][algo_name].setdefault(path, []).append(new_path)

        results['Potential'] = sorted(list(results['Potential']))
        return results


class Apple:
    pass


class Orange:
    def __init__(self, number):
        self.number = number


class Lemon:
    pass


class FirstAlgorithm:
    SPECIFICATION = {
        Orange: [Apple],
        Lemon: [Orange, Apple]
    }

    def __call__(self, source_object):
        if isinstance(source_object, Orange):
            return [Apple() for _ in range(source_object.number)]
        if isinstance(source_object, Lemon):
            return [Orange(3), Apple()]
        return []


class EmptyAlgorithm:
    SPECIFICATION = {}

    def __call__(self, source_object):
        return []


if __name__ == '__main__':
    component = Component(FirstAlgorithm(), EmptyAlgorithm())
    print(json.dumps(
        component.search_algorithm(Lemon),
        indent=4
    ))
